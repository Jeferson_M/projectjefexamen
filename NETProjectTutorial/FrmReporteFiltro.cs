﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmReporteFiltro : Form
    {

        private DataSet dsSistema;
        public FrmReporteFiltro()
        {
            InitializeComponent();
        }
        public DataSet DsSistema
        {
            set
            {
                dsSistema = value;
            }
        }

        private void FrmReporteFiltro_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }

        private void reportViewer1_Load(object sender, EventArgs e)
        {

        }
    }
}
