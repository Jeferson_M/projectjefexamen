﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmFiltroFactura : Form
    {
        private DataSet dsFacturas;
        private BindingSource bsFacturas;
        private DataSet dsSistema;
        private BindingSource bsProductoFactura;

        public FrmFiltroFactura()
        {
            InitializeComponent();
            bsProductoFactura = new BindingSource();
        }

        public DataSet DsFacturas
        {
            get
            {
                return dsFacturas;
            }

            set
            {
                dsFacturas = value;
            }
        }

        public DataSet DsSistema
        {
            set
            {
                dsSistema = value;
            }
        }


        private void FrmFiltroFactura_Load(object sender, EventArgs e)
        {
            cmbFiltro.DataSource = dsSistema.Tables["ReporteFactura"];
            cmbFiltro.DisplayMember = "Nombre_Empleado";
            cmbFiltro.ValueMember = "Cod_Factura";

            dsSistema.Tables["FiltroFactura"].Rows.Clear();
            bsProductoFactura.DataSource = dsSistema.Tables["FiltroFactura"];
            dgvFiltro.DataSource = bsProductoFactura;

        }

        private void cmbFiltro_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            try
            {
                DataRow drFiltro = ((DataRowView)cmbFiltro.SelectedItem).Row;
                DataRow drFiltroFactura = dsSistema.Tables["FiltroFactura"].NewRow();
                drFiltroFactura["Codigo"] = drFiltro["SKU"];
                drFiltroFactura["Fecha"] = drFiltro["Fecha"];
                drFiltroFactura["Subtotal"] = drFiltro["Subtotal"];
                drFiltroFactura["IVA"] = drFiltro["IVA"];
                drFiltroFactura["Total"] = drFiltro["Total"];
                dsSistema.Tables["FiltroFactura"].Rows.Add(drFiltroFactura);
            }
            catch (ConstraintException)
            {
                MessageBox.Show(this, "ERROR, producto ya agregado, verifique por favor!",
                    "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnVer_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = dgvFiltro.SelectedRows;

            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "Debe seleccionar una fila de la tabla para poder ver", "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            DataGridViewRow gridRow = rowCollection[0];
            DataRow drow = ((DataRowView)gridRow.DataBoundItem).Row;
            FrmReporteFiltro frf = new FrmReporteFiltro();
            frf.DsSistema = DsFacturas;
            frf.Show();

        }
    }
}
