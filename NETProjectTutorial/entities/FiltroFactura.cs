﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.entities
{
    class FiltroFactura
    {

        private string cod_factura;
        private DateTime fecha;
        private double subtotal;
        private double iva;
        private double total;

        public string Cod_factura
        {
            get
            {
                return cod_factura;
            }

            set
            {
                cod_factura = value;
            }
        }

        public DateTime Fecha
        {
            get
            {
                return fecha;
            }

            set
            {
                fecha = value;
            }
        }

        public double Subtotal
        {
            get
            {
                return subtotal;
            }

            set
            {
                subtotal = value;
            }
        }

        public double Iva
        {
            get
            {
                return iva;
            }

            set
            {
                iva = value;
            }
        }

        public double Total
        {
            get
            {
                return total;
            }

            set
            {
                total = value;
            }
        }

        public FiltroFactura(string cod_factura, DateTime fecha, double subtotal, double iva, double total)
        {
            this.Cod_factura = cod_factura;
            this.Fecha = fecha;
            this.Subtotal = subtotal;
            this.Iva = iva;
            this.Total = total;
        }
    }
}
